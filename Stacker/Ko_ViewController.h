//
//  Ko_ViewController.h
//  Stacker
//
//  Created by Tietronix Software on 8/22/13.
//
//

#import <UIKit/UIKit.h>

#define SHADOWONGROUNDENABLED 0
#define SHADOWONBUILDINGENABLED 1

enum GameLoopState {
    GS_NOT_RUNNING=0,
    GS_NORMAL_LOOP,
    GS_PIECE_DROPPING,
    GS_PIECE_DROPPING_GOOD,
    GS_PIECE_DROPPING_BAD
};

enum ScreenSide {
    LEFTSIDE,
    RIGHTSIDE
};

@interface Ko_ViewController : UIViewController {
    enum GameLoopState gamestate;
    enum ScreenSide lastSpawnSide;
    float errorAllowed;
    float timeWithinZone;
    UIView *buildingContainer;
    UIView *groundView;
    UIView *nextFloorView;
#if SHADOWONBUILDINGENABLED == 1
    UIView *shadowOnBuilding;
#endif
#if SHADOWONGROUNDENABLED == 1
    UIView *shadowOnGround;
#endif
    
    BOOL dropFloor;
    
    float leftDropError;
    float rightDropError;
    
    int currentWidthInBlocks;
    CGPoint currentSpeed;
    
    int numOfFloors;
    int score;
    
    NSTimer *gameLoopTimer;

    UIView *titleScreen;

    UIView *scoreScreen;
    UILabel *floorCountLabel;
    UILabel *scoreLabel;
    
    UIView *finalBuildingView;
    UIScrollView *finalBuildingScroll;
    NSMutableArray *floorArray;
}

@end

BOOL randomBool();
