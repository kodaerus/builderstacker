//
//  Ko_AppDelegate.h
//  Stacker
//
//  Created by Tietronix Software on 8/22/13.
//
//

#import <UIKit/UIKit.h>

@class Ko_ViewController;

@interface Ko_AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) Ko_ViewController *viewController;

@end
