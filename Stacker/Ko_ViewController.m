//
//  Ko_ViewController.m
//  Stacker
//
//  Created by Tietronix Software on 8/22/13.
//
//

#import "Ko_ViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface Ko_ViewController ()
@end

@implementation Ko_ViewController

const float blockSizeWidth = 16.f;
const float blockSizeHeight = 24.f;
const float fallSpeed = 24.f;
const float deltaTime = 0.05f;

#define SPAWNTYPEANYWHERE 0
#define SPAWNTYPEEDGES 1
#define SPAWNTYPEINVALIDREGION 2
#define SPAWNTYPEENABLED SPAWNTYPEEDGES

#define SPAWNDIRECTIONCENTER 0
#define SPAWNDIRECTIONLAST 1
#define SPAWNDIRECTIONLASTRANDOM 2
#define SPAWNDIRECTIONRANDOM 3
#define SPAWNDIRECTIONENABLED SPAWNDIRECTIONCENTER
#if SPAWNDIRECTIONENABLED == SPAWNDIRECTIONLASTRANDOM
const int oddsOfChangingDirection = 71;
#endif

#define SNAPENABLED 0
#if SNAPENABLED == 1
const float snapSize = blockSizeWidth * 0.5f;
#endif

#define DYNAMICXSPEEDENABLED 1
#if DYNAMICXSPEEDENABLED == 1
const float maxXSpeed = 192.f;
const int floorModuloForXStep = 3;
const float xSpeedSteps = 8.f;
#endif
const float startXSpeed = 32.f;

#define DYNAMICYSPEEDENABLED 1
#if DYNAMICYSPEEDENABLED == 1
const float maxYSpeed = 240.f;
const int floorModuloForYStep = 4;
const float ySpeedSteps = 12.f;
#endif
const float startYSpeed = 24.f;

#define DYNAMICERRORENABLED 1
#if DYNAMICERRORENABLED == 1
const int floorModuloForErrorShrink = 10;
const float errorShrinkStep = 2.f;
const float minErrorAllowed = 6.f;
#endif
const float maxErrorAllowed = 12.f;

#define DYNAMICFLOORSIZEENABLED 1
#if DYNAMICFLOORSIZEENABLED == 1
const int floorModuloForBlockSizeStep = 8;
#endif
const int startFloorSize = 10;

#define DYNAMICSCREENSIZEENABLED 0
#if DYNAMICSCREENSIZEENABLED == 1
const int floorModuloForScreenMove = 2;
const float screenMoveStep = blockSizeHeight * 0.2f;
#endif
const int minPlayZoneScreenSize = 240;
const int playZoneScreenSize = 240;

#define SETFLOORCOLOR [UIColor brownColor]
#define SETFLOORBORDERCOLOR [UIColor blackColor]
#define NEXTFLOORVALIDCOLOR [UIColor lightGrayColor]
#define NEXTFLOORINVALIDCOLOR [UIColor redColor]
#define NEXTFLOORBORDERCOLOR [UIColor blackColor]
#define BACKGROUNDCOLOR [UIColor cyanColor]

- (CAGradientLayer*)makeShadowLayer {
    CAGradientLayer *maskLayer = [CAGradientLayer layer];
    maskLayer.anchorPoint = CGPointZero;
    
    //maskLayer.startPoint = CGPointMake(0.0f, 0.0f);
    //maskLayer.endPoint = CGPointMake(1.f, 1.0f);
    
    UIColor *outerColor = [UIColor colorWithWhite:1.0 alpha:0.8];
    UIColor *innerColor = [UIColor colorWithWhite:1.0 alpha:0.f];
    
    maskLayer.colors = @[(id)outerColor.CGColor, (id)outerColor.CGColor, (id)innerColor.CGColor, (id)innerColor.CGColor];
    maskLayer.locations = @[@0.0, @0.15, @0.8, @1.0f];
    return maskLayer;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:BACKGROUNDCOLOR];
	// Do any additional setup after loading the view, typically from a nib.
    gamestate = GS_NOT_RUNNING;
    lastSpawnSide = LEFTSIDE;
    errorAllowed = maxErrorAllowed;
    timeWithinZone = 0.f;
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    groundView = [[UIView alloc] initWithFrame:CGRectMake(0.f, playZoneScreenSize, screenBounds.size.width, screenBounds.size.height-minPlayZoneScreenSize)];
    [groundView setBackgroundColor:[UIColor greenColor]];
    [self.view addSubview:groundView];
    
    buildingContainer = [[UIView alloc] initWithFrame:groundView.frame];
    buildingContainer.layer.borderColor = [[UIColor redColor] CGColor];
    buildingContainer.layer.borderWidth = 1.f;
    [self.view addSubview:buildingContainer];
    
    int maxFloors = (int)ceilf(buildingContainer.bounds.size.height/blockSizeHeight);
    for (int i=0; i < maxFloors; ++i) {
        float w = blockSizeWidth*(i/2+1);
        if (w < blockSizeWidth) {
            w = blockSizeWidth;
        }
        if (w > startFloorSize*blockSizeWidth) {
            w = startFloorSize*blockSizeWidth;
        }
        float h = blockSizeHeight;
        float x = buildingContainer.bounds.size.width/2.f - w/2;
        float y = i*blockSizeHeight;
        UIView *floor = [self makeFloorAtRect:CGRectMake(x, y, w, h)];
        [floor setBackgroundColor:SETFLOORCOLOR];
        [buildingContainer addSubview:floor];
    }

    currentWidthInBlocks = startFloorSize;
    currentSpeed = CGPointMake(startXSpeed, startYSpeed);
    
    nextFloorView = [self makeFloorAtRect:CGRectMake(buildingContainer.bounds.size.width/2.f - (blockSizeWidth*currentWidthInBlocks)/2.f, 5.f, blockSizeWidth*currentWidthInBlocks, blockSizeHeight)];
    [nextFloorView setBackgroundColor:NEXTFLOORVALIDCOLOR];
    nextFloorView.layer.borderColor = [NEXTFLOORBORDERCOLOR CGColor];
    [self.view addSubview:nextFloorView];
    
#if SHADOWONBUILDINGENABLED == 1
    shadowOnBuilding = [[UIView alloc] initWithFrame:CGRectMake(0.f, 0.f, 0.f, blockSizeHeight)];
    [shadowOnBuilding setBackgroundColor:[UIColor darkGrayColor]];
    //shadowOnBuilding.layer.cornerRadius = blockSizeHeight/2.f;
    //shadowOnBuilding.layer.mask = [CALayer layer];
    //shadowOnBuilding.layer.mask.backgroundColor = [[UIColor blackColor] CGColor];
    shadowOnBuilding.layer.mask = [self makeShadowLayer];
    shadowOnBuilding.layer.mask.frame = shadowOnBuilding.bounds;
    [buildingContainer addSubview:shadowOnBuilding];
#endif
#if SHADOWONGROUNDENABLED == 1
    shadowOnGround = [[UIView alloc] initWithFrame:CGRectMake(0.f, 0.f, 0.f, blockSizeHeight)];
    [shadowOnGround setBackgroundColor:[UIColor darkGrayColor]];
    //shadowOnGround.layer.cornerRadius = blockSizeHeight/2.f;
    //shadowOnGround.layer.mask = [CALayer layer];
    //shadowOnGround.layer.mask.backgroundColor = [[UIColor blackColor] CGColor];
    shadowOnGround.layer.mask = [self makeShadowLayer];
    shadowOnGround.layer.mask.frame = shadowOnGround.bounds;
    [buildingContainer addSubview:shadowOnGround];
#endif

    finalBuildingScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(screenBounds.size.width/2.f-130.f, screenBounds.size.height * 0.5f, 260.f, screenBounds.size.height * 0.5f - 30.f)];
    [finalBuildingScroll setBackgroundColor:[UIColor whiteColor]];
    finalBuildingScroll.layer.borderColor = [[UIColor blackColor] CGColor];
    finalBuildingScroll.layer.borderWidth = 1.f;
    finalBuildingScroll.layer.cornerRadius = 15.f;

    finalBuildingView = [[UIView alloc] initWithFrame:CGRectMake(0.f, 0.f, finalBuildingScroll.frame.size.width, 0.f)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15.f, 15.f, 230.f, 20.f)];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = @"Final Building";
    label.font = [UIFont systemFontOfSize:18.f];
    [finalBuildingView addSubview:label];
    [label release];
    label = nil;
    [finalBuildingScroll addSubview:finalBuildingView];
    [self.view addSubview:finalBuildingScroll];
    finalBuildingScroll.hidden = YES;

    floorArray = [[NSMutableArray alloc] init];
    
    scoreScreen = [[UIView alloc] initWithFrame:CGRectMake(screenBounds.size.width/2.f-130.f, 30.0, 260.f, 200.f)];
    [scoreScreen setBackgroundColor:[UIColor whiteColor]];
    scoreScreen.layer.borderColor = [[UIColor blackColor] CGColor];
    scoreScreen.layer.borderWidth = 1.f;
    scoreScreen.layer.cornerRadius = 15.f;

    label = [[UILabel alloc] initWithFrame:CGRectMake(15.f, 15.f, 230.f, 20.f)];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = @"Final Score";
    label.font = [UIFont systemFontOfSize:18.f];
    [scoreScreen addSubview:label];
    [label release];
    label = nil;
    
    label = [[UILabel alloc] initWithFrame:CGRectMake(15.f, 125.f, 150.f, 20.f)];
    label.textAlignment = NSTextAlignmentRight;
    label.text = @"Final Floor Count:";
    label.font = [UIFont systemFontOfSize:14.f];
    [scoreScreen addSubview:label];
    [label release];
    label = nil;

    floorCountLabel = [[UILabel alloc] initWithFrame:CGRectMake(175.f, 125.f, 70.f, 20.f)];
    floorCountLabel.textAlignment = NSTextAlignmentLeft;
    floorCountLabel.font = [UIFont systemFontOfSize:14.f];
    [scoreScreen addSubview:floorCountLabel];

    label = [[UILabel alloc] initWithFrame:CGRectMake(15.f, 145.f, 150.f, 20.f)];
    label.textAlignment = NSTextAlignmentRight;
    label.text = @"Final Score:";
    label.font = [UIFont systemFontOfSize:14.f];
    [scoreScreen addSubview:label];
    [label release];
    label = nil;

    scoreLabel = [[UILabel alloc] initWithFrame:CGRectMake(175.f, 145.f, 70.f, 20.f)];
    scoreLabel.textAlignment = NSTextAlignmentLeft;
    scoreLabel.font = [UIFont systemFontOfSize:14.f];
    [scoreScreen addSubview:scoreLabel];
    
    label = [[UILabel alloc] initWithFrame:CGRectMake(15.f, 170.f, 230.f, 15.f)];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = @"Tap to begin new game";
    label.font = [UIFont systemFontOfSize:12.f];
    [scoreScreen addSubview:label];
    [label release];
    label = nil;
    
    [self.view addSubview:scoreScreen];
    scoreScreen.hidden = YES;
    
    titleScreen = [[UIView alloc] initWithFrame:CGRectMake(screenBounds.size.width/2.f-130.f, 30.0, 260.f, 200.f)];
    [titleScreen setBackgroundColor:[UIColor whiteColor]];
    titleScreen.layer.borderColor = [[UIColor blackColor] CGColor];
    titleScreen.layer.borderWidth = 1.f;
    titleScreen.layer.cornerRadius = 15.f;
    
    label = [[UILabel alloc] initWithFrame:CGRectMake(15.f, 15.f, 230.f, 20.f)];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = @"BUILDER!";
    label.font = [UIFont systemFontOfSize:18.f];
    [titleScreen addSubview:label];
    [label release];
    label = nil;
    
    label = [[UILabel alloc] initWithFrame:CGRectMake(15.f, 170.f, 230.f, 15.f)];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = @"Tap to begin new game";
    label.font = [UIFont systemFontOfSize:12.f];
    [titleScreen addSubview:label];
    [label release];
    label = nil;
    
    [self.view addSubview:titleScreen];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapScreen)];
    tap.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:tap];
    [tap release];
}

- (void)dealloc {
    [super dealloc];
    [gameLoopTimer invalidate]; [gameLoopTimer release]; gameLoopTimer = nil;
    [buildingContainer release]; buildingContainer = nil;
    [groundView release]; groundView = nil;
#if SHADOWONGROUNDENABLED == 1
    [shadowOnGround release]; shadowOnGround = nil;
#endif
    [nextFloorView release]; nextFloorView = nil;
#if SHADOWONBUILDINGENABLED == 1
    [shadowOnBuilding release]; shadowOnBuilding = nil;
#endif
    [scoreScreen release]; scoreScreen = nil;
    [floorCountLabel release]; floorCountLabel = nil;
    [scoreLabel release]; scoreLabel = nil;
    [titleScreen release]; titleScreen = nil;
    [finalBuildingView release]; finalBuildingView = nil;
    [finalBuildingScroll release]; finalBuildingScroll = nil;
    [floorArray release]; floorArray = nil;
}

- (BOOL)isGameRunning {
    return gameLoopTimer != nil;
}

- (UIView*)makeFloorAtRect:(CGRect)r {
    UIView *floor = [[UIView alloc] initWithFrame:r];
    floor.layer.borderColor = [NEXTFLOORBORDERCOLOR CGColor];
    floor.layer.borderWidth = 1.f;
    return floor;
}

- (void)setupAndSpawnFloor {
    CGRect spawnPoint = nextFloorView.frame;
    spawnPoint.size.width = blockSizeWidth*currentWidthInBlocks;
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
#if SPAWNTYPEENABLED == SPAWNTYPEANYWHERE
    int rangeSize = buildingContainer.frame.size.width - spawnPoint.size.width;
    int randomX = arc4random() % rangeSize;
#elif SPAWNTYPEENABLED == SPAWNTYPEEDGES
    int randomX = 0;
    //if (randomBool()) {
        //randomX = lastSpawnSide == RIGHTSIDE ? screenBounds.size.width-spawnPoint.size.width : 0;
    //} else {
        randomX = lastSpawnSide == LEFTSIDE ? screenBounds.size.width-spawnPoint.size.width : 0;
    //}
#elif SPAWNTYPEENABLED == SPAWNTYPEINVALIDREGION
    int rangeSize = buildingContainer.frame.size.width - spawnPoint.size.width;
    int startEmpty = rangeSize;
    int emptySize = 0;
    UIView *topFloor = [buildingContainer.subviews objectAtIndex:0];
    if (topFloor.frame.size.width > 0) {
        startEmpty = topFloor.frame.origin.x + errorAllowed;
        emptySize = topFloor.frame.size.width - errorAllowed*2.f;
        rangeSize = rangeSize - emptySize;
    }
    
    int randomX = arc4random() % rangeSize;
    //NSLog(@"Range size: %d, Randomx: %d", rangeSize, randomX);
    if (randomX+spawnPoint.size.width > startEmpty) {
        randomX += emptySize;
        //NSLog(@"Adjusted Randomx: %d", randomX);
    }
#endif
    
    spawnPoint.origin.x = randomX;
    spawnPoint.origin.y = 0.f;
    nextFloorView.frame = spawnPoint;
    
    UIView *topFloor = [buildingContainer.subviews objectAtIndex:0];
    float centerX = topFloor.frame.origin.x+(topFloor.frame.size.width/2.f);
    if (topFloor.frame.size.width < 0) {
        centerX = screenBounds.size.width/2.f;
    }

#if SHADOWONBUILDINGENABLED == 1
    spawnPoint.origin.y = 0;
    shadowOnBuilding.frame = spawnPoint;
#endif

#if SHADOWONGROUNDENABLED == 1
    spawnPoint.origin.y = shadowOnGround.frame.origin.y;
    shadowOnGround.frame = spawnPoint;
#endif
    [self computeShadowClips:topFloor];

#if SPAWNDIRECTIONENABLED == SPAWNDIRECTIONCENTER
    currentSpeed.x = ABS(currentSpeed.x);
    if (spawnPoint.origin.x+spawnPoint.size.width > centerX) {
        currentSpeed.x = -currentSpeed.x;
    } else if (spawnPoint.origin.x+spawnPoint.size.width == centerX) {
        if (randomBool() == 0) {
            currentSpeed.x = -currentSpeed.x;
        }
    }

#elif SPAWNDIRECTIONENABLED == SPAWNDIRECTIONLASTRANDOM
    if (arc4random() % 100 <= oddsOfChangingDirection) {
        currentSpeed.x = -currentSpeed.x;
    }
#elif SPAWNDIRECTIONENABLED == SPAWNDIRECTIONRANDOM
    currentSpeed.x = ABS(currentSpeed.x);
    if (randomBool() == 0) {
        currentSpeed.x = -currentSpeed.x;
    }
#endif

    if (spawnPoint.origin.x > 0) {
        lastSpawnSide = RIGHTSIDE;
    } else {
        lastSpawnSide = LEFTSIDE;
    }

    dropFloor = NO;
}

- (void)shiftFloors {
#if SHADOWONGROUNDENABLED == 1
    CGRect shadowR = shadowOnGround.frame;
    shadowR.origin.y += blockSizeHeight;
    shadowOnGround.frame = shadowR;
#endif
    
    for (int i=[buildingContainer.subviews count]-1; i >= 0; --i) {
        UIView *floor = [buildingContainer.subviews objectAtIndex:i];
        UIView *floorAbove = i > 0 ? [buildingContainer.subviews objectAtIndex:i-1] : nextFloorView;
        
        CGRect r = floorAbove.frame;
        r.origin.y = floor.frame.origin.y;
        floor.frame = r;
    }

    CGRect containerR = groundView.frame;
    if (containerR.origin.y < buildingContainer.frame.origin.y + groundView.frame.size.height) {
        containerR.origin.y += blockSizeHeight;
        groundView.frame = containerR;
    }
}

- (void)startNewGame {
    [gameLoopTimer invalidate]; [gameLoopTimer release]; gameLoopTimer = nil;
    scoreScreen.hidden = YES;
    titleScreen.hidden = YES;
    finalBuildingScroll.hidden = YES;
    buildingContainer.hidden = NO;
#if SHADOWONBUILDINGENABLED == 1
    shadowOnBuilding.hidden = NO;
#endif
#if SHADOWONGROUNDENABLED == 1
    shadowOnGround.hidden = NO;
#endif
    [floorArray removeAllObjects];
    errorAllowed = maxErrorAllowed;
    numOfFloors = 0;
    score = 0;
    currentWidthInBlocks = startFloorSize;
    currentSpeed = CGPointMake(startXSpeed, startYSpeed);

    NSArray *floorViews = finalBuildingView.subviews;
    for (UIView *floor in floorViews) {
        if (![floor isKindOfClass:[UILabel class]]) {
            [floor removeFromSuperview];
        }
    }
    
    CGRect containerR = buildingContainer.frame;
    containerR.origin.y = playZoneScreenSize;
    buildingContainer.frame = containerR;
    groundView.frame = containerR;
    
#if SHADOWONGROUNDENABLED == 1
    containerR = shadowOnGround.frame;
    containerR.origin.y = 0;
    shadowOnGround.frame = containerR;
#endif

    for (UIView *floor in buildingContainer.subviews) {
        floor.frame = CGRectMake(0.f, floor.frame.origin.y, 0.f, blockSizeHeight);
    }
    [self setupAndSpawnFloor];
    gamestate = GS_NORMAL_LOOP;
    gameLoopTimer = [[NSTimer scheduledTimerWithTimeInterval:deltaTime target:self selector:@selector(gameLoop) userInfo:nil repeats:YES] retain];
}

- (void)endGame {
    [gameLoopTimer invalidate]; [gameLoopTimer release]; gameLoopTimer = nil;
    gamestate = GS_NOT_RUNNING;
#if SHADOWONBUILDINGENABLED == 1
    shadowOnBuilding.hidden = YES;
#endif
#if SHADOWONGROUNDENABLED == 1
    shadowOnGround.hidden = NO;
#endif
    dropFloor = NO;
    //NSLog(@"Reached floors: %d", numOfFloors);
    scoreScreen.hidden = NO;
    floorCountLabel.text = [NSString stringWithFormat:@"%d", numOfFloors];
    scoreLabel.text = [NSString stringWithFormat:@"%d", score];

    if ([floorArray count] > 0) {
        NSValue *value = [floorArray objectAtIndex:0];
        CGRect r = [value CGRectValue];
        float offset = (finalBuildingView.frame.size.width/2.f - r.size.width/2.f) - (r.origin.x);
        for (int i=0; i < [floorArray count]; ++i) {
            value = [floorArray objectAtIndex:i];
            r = [value CGRectValue];
            r.origin.x += offset;
            r.origin.y = ([floorArray count] - i - 1) * blockSizeHeight + 45.f;
            UIView *floor = [self makeFloorAtRect:r];
            [floor setBackgroundColor:SETFLOORCOLOR];
            [finalBuildingView addSubview:floor];
            [floor release];
        }
    }
    CGSize size = CGSizeMake(finalBuildingView.frame.size.width, [floorArray count]*blockSizeHeight+60.f);
    CGRect r = finalBuildingView.frame;
    r.size = size;
    finalBuildingView.frame = r;
    
    finalBuildingScroll.contentSize = size;

    finalBuildingScroll.hidden = NO;
    buildingContainer.hidden = YES;
}

- (float)calculateFloorErrors {
    UIView *topFloor = [buildingContainer.subviews objectAtIndex:0];
    float maxError = 0;
    if (numOfFloors > 0) {
        leftDropError = topFloor.frame.origin.x - nextFloorView.frame.origin.x;
        rightDropError = (nextFloorView.frame.origin.x+nextFloorView.frame.size.width) - (topFloor.frame.origin.x+topFloor.frame.size.width);
        maxError = MAX(leftDropError, rightDropError);
    }
    
    return maxError;
}

- (void)computeShadowClips:(UIView*)topFloor {
    [CATransaction begin];
    [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
#if SHADOWONBUILDINGENABLED == 1
    CGRect intersect = CGRectIntersection(shadowOnBuilding.frame, topFloor.frame);
    intersect.origin.x -= shadowOnBuilding.frame.origin.x;
    shadowOnBuilding.layer.mask.frame = intersect;
#endif
    
#if SHADOWONGROUNDENABLED == 1
    if (shadowOnGround.frame.origin.y < buildingContainer.frame.size.height) {
        CGRect intersect = shadowOnGround.frame;
        intersect.origin.y = 0;
        intersect = CGRectIntersection(intersect, topFloor.frame);
        if (intersect.origin.x > shadowOnGround.bounds.size.width) {
            intersect = shadowOnGround.bounds;
        } else if (intersect.origin.x > shadowOnGround.frame.origin.x) {
            intersect = CGRectMake(0.f, 0.f, intersect.origin.x-shadowOnGround.frame.origin.x, intersect.size.height);
        } else if (intersect.size.width < shadowOnGround.frame.size.width) {
            intersect = CGRectMake(intersect.size.width, 0.f, shadowOnGround.frame.size.width-intersect.size.width, intersect.size.height);
        } else {
            NSLog(@"3");
            intersect = shadowOnGround.bounds;
        }
        shadowOnGround.layer.mask.frame = intersect;
    }
#endif
    [CATransaction commit];
}

- (void)gameLoop {
    enum GameLoopState state = gamestate;
    CGRect newRect = nextFloorView.frame;
    
    float maxError = [self calculateFloorErrors];
    
    //[nextFloorView setBackgroundColor:maxError < errorAllowed ? NEXTFLOORVALIDCOLOR : NEXTFLOORINVALIDCOLOR];
    UIView *topFloor = [buildingContainer.subviews objectAtIndex:0];
    
    switch (state) {
        case GS_NOT_RUNNING:
        default:
            break;
        case GS_NORMAL_LOOP:
        {
            float moveX = currentSpeed.x * deltaTime;
            float moveY = currentSpeed.y * deltaTime;
            if ((currentSpeed.x > 0 && newRect.origin.x + newRect.size.width + moveX > buildingContainer.bounds.size.width)
                || (currentSpeed.x < 0 && newRect.origin.x + moveX < 0.f)) {
                currentSpeed.x = -currentSpeed.x;
            }
            newRect.origin.x += moveX;
            newRect.origin.y += moveY;
            nextFloorView.frame = newRect;
            
            if (newRect.origin.y+blockSizeHeight >= buildingContainer.frame.origin.y) {
                //NSLog(@"FAIL, waited too long!");
                [self endGame];
            }

#if SHADOWONBUILDINGENABLED == 1
            newRect = shadowOnBuilding.frame;
            newRect.origin.x = nextFloorView.frame.origin.x;
            shadowOnBuilding.frame = newRect;
#endif
#if SHADOWONGROUNDENABLED == 1
            newRect = shadowOnGround.frame;
            newRect.origin.x = nextFloorView.frame.origin.x;
            shadowOnGround.frame = newRect;
#endif
            
            [self computeShadowClips:topFloor];
            break;
        }
        case GS_PIECE_DROPPING:
        {
            if (maxError < errorAllowed) {
                //NSLog(@"NAILED IT!");
                
                if (numOfFloors > 0 && maxError > 0) {
                    CGRect r = nextFloorView.frame;
                    if (maxError == leftDropError) {
                        r.origin.x = topFloor.frame.origin.x;
                    } else {
                        r.origin.x = topFloor.frame.origin.x + topFloor.frame.size.width - nextFloorView.frame.size.width;
                    }
#if SNAPENABLED == 1
                    r.origin.x = snapSize * roundf(r.origin.x / snapSize);
#endif
                    nextFloorView.frame = r;
                }
#if SHADOWONBUILDINGENABLED == 1
                newRect = shadowOnBuilding.frame;
                newRect.origin.x = nextFloorView.frame.origin.x;
                shadowOnBuilding.frame = newRect;
#endif
#if SHADOWONGROUNDENABLED == 1
                newRect = shadowOnGround.frame;
                newRect.origin.x = nextFloorView.frame.origin.x;
                shadowOnGround.frame = newRect;
#endif
                gamestate = GS_PIECE_DROPPING_GOOD;
            } else {
                gamestate = GS_PIECE_DROPPING_BAD;
            }
            break;
        }
        case GS_PIECE_DROPPING_GOOD:
        {
            CGRect r = nextFloorView.frame;
            if (r.origin.y+blockSizeHeight < buildingContainer.frame.origin.y) {
                r.origin.y += fallSpeed;
                if (r.origin.y+blockSizeHeight > buildingContainer.frame.origin.y) {
                    r.origin.y = buildingContainer.frame.origin.y-blockSizeHeight;
                }
                nextFloorView.frame = r;
            } else {
                gamestate = GS_NORMAL_LOOP;
                NSValue *value = [NSValue valueWithCGRect:nextFloorView.frame];
                [floorArray addObject:value];
                numOfFloors++;
                score = numOfFloors * (int)(maxErrorAllowed - maxError);
                
                [self shiftFloors];
                // is it time to decrease the floor size?
#if DYNAMICFLOORSIZEENABLED == 1
                if (numOfFloors % floorModuloForBlockSizeStep == 0) {
                    if (currentWidthInBlocks > 1) {
                        currentWidthInBlocks--;
                    } else {
                        NSLog(@"Min width already reached");
                    }
                }
#endif
                // or how bout speed adjustments? etc
#if DYNAMICXSPEEDENABLED == 1
                if (numOfFloors % floorModuloForXStep == 0) {
                    if (fabsf(currentSpeed.x) < maxXSpeed) {
                        if (currentSpeed.x < 0) {
                            currentSpeed.x = currentSpeed.x - xSpeedSteps;
                        } else {
                            currentSpeed.x = currentSpeed.x + xSpeedSteps;
                        }
                    } else {
                        NSLog(@"Max X speed already reached");
                    }
                }
#endif
#if DYNAMICYSPEEDENABLED == 1
                if (numOfFloors % floorModuloForYStep == 0) {
                    if (fabsf(currentSpeed.y) < maxYSpeed) {
                        currentSpeed.y = fabsf(currentSpeed.y) + ySpeedSteps;
                    } else {
                        NSLog(@"Max Y speed already reached");
                    }
                }
#endif
                // floor location adjustment?
#if DYNAMICSCREENSIZEENABLED == 1
                if (numOfFloors % floorModuloForScreenMove == 0) {
                    if (buildingContainer.frame.origin.y > minPlayZoneScreenSize) {
                        CGRect containerR = buildingContainer.frame;
                        containerR.origin.y -= screenMoveStep;
                        buildingContainer.frame = containerR;
                    } else {
                        NSLog(@"Min play screen size already reached");
                    }
                }
#endif
                // finally, error allowed adjustment!
#if DYNAMICERRORENABLED == 1
                if (numOfFloors % floorModuloForErrorShrink == 0) {
                    if (errorAllowed > minErrorAllowed) {
                        errorAllowed -= errorShrinkStep;
                    } else {
                        NSLog(@"Min error allowed already reached");
                    }
                }
#endif
                [self setupAndSpawnFloor];
            }
            break;
        }
        case GS_PIECE_DROPPING_BAD:
        {
            CGRect r = nextFloorView.frame;
            if (r.origin.y+blockSizeHeight < buildingContainer.frame.origin.y) {
                r.origin.y += fallSpeed;
                if (r.origin.y+blockSizeHeight > buildingContainer.frame.origin.y) {
                    r.origin.y = buildingContainer.frame.origin.y-blockSizeHeight;
                }
                nextFloorView.frame = r;
            } else {
                gamestate = GS_NORMAL_LOOP;
                //NSLog(@"FAIL, missed the target!");
                [self endGame];
            }
            break;
        }
    }
}

- (void)dropFloor {
    gamestate = GS_PIECE_DROPPING;
    dropFloor = YES;
}

- (void)tapScreen {
    if ([self isGameRunning] && gamestate == GS_NORMAL_LOOP) {
        [self dropFloor];
    } else {
        [self startNewGame];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

BOOL randomBool() {
    int tmp = (arc4random() % 30)+1;
    if(tmp % 5 == 0)
        return YES;
    return NO;
}
