//
//  main.m
//  Stacker
//
//  Created by Tietronix Software on 8/22/13.
//
//

#import <UIKit/UIKit.h>

#import "Ko_AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([Ko_AppDelegate class]));
    }
}
